namespace :pact do
    desc 'Publish particular pact contract to Pact Broker'
    task publish: :environment do
      # PACT_APP_VERSION=2.0.0 PACT_BROKER_URL=http://pact-broker.ahinternal.net rake pact:publish
      raise 'Proper semantic PACT_APP_VERSION is required' unless ENV['PACT_APP_VERSION'].match?(/^\d+\.\d+\.\d+/)
  
      PactBroker::Client::PublicationTask.new do |task|
        task.pattern = 'spec/pacts/*.json'
        task.consumer_version = ENV['PACT_APP_VERSION']
        task.pact_broker_base_url = ENV['PACT_BROKER_URL']
  
        if ENV['PACT_BROKER_USERNAME'] && ENV['PACT_BROKER_PASSWORD'] # optional
          task.pact_broker_basic_auth = {
            username:  ENV['PACT_BROKER_USERNAME'],
            password: ENV['PACT_BROKER_PASSWORD']
          }
        end
      end
    end
  end