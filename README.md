# Weather application

Weather application shows temperature from various points in world. 

## Milestone 1

In this milestone our app should allow user to:
- select longitude, latitude
- show list of cities with temperature in

It should communicate with two services:

1. City Finder:
    - returns list of cities in radius from provided longitude, latitude
1. Temperature Info:
    - returns temperature in city

## App installation

1. Install BASH
1. Install `rbenv`
1. Install `rails`
1. Install `rubocop`
1. Clone repo https://bitbucket.org/nczita/weather
1. Run `rails . new -T` and select `n` when asking for overwrite files.

Or:
1. Go to https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one
1. Follow the instruction there

Note:

Change in `Gemfile:9`:

```ruby
gem 'sqlite3', '~> 1.3.6'
```

and then:

```bash
bundle install
```


## PACT

To run pact tests, run rspec:

```sh
rspec
```

To verify pact contract, run:

```sh
rake pact:verify

# or

PACT_BROKER_URL=http://pact-broker.ahinternal.net rake pact:verify
```

To publish:

```sh
export PACT_BROKER_URL=http://pact-broker.ahinternal.net
export PACT_APP_VERSION=1.0.0
export RACK_ENV=development
# or
export RACK_ENV=tests
# remember of lib/tasks/pact.rake !!!
rake pact:publish
```