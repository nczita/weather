class TemperatureController < ApplicationController
    # GET /api/temperature
    def show
        json_response({city: "Warszawa", temp: 3}, :ok)
    end
end