class CitiesController < ApplicationController
    # GET /api/cities
    def show
        json_response(["Gdansk", "Sopot", "Wejherowo", "Sztum", "Gdynia"], :ok)
    end
end