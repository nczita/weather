class WeatherController < ApplicationController
  # GET /weather
  def show
    @status = :ok
    if weather_params.empty?
      @weathers = []
    else
      if valid_params?
        @weathers = WeatherService
                    .new(weather_params[:longitude],
                         weather_params[:latitude])
                    .call
      else
        @weathers = { error: 'Invalid params', id: 'CKE-123' }
        @status = :unprocessable_entity
      end
    end
    json_response(@weathers, @status)
  end

  private

  def valid_params?
    (weather_params[:longitude].to_f >= 0) &&
      (weather_params[:longitude].to_f <= 180) &&
      (weather_params[:latitude].to_f >= 0) &&
      (weather_params[:latitude].to_f <= 90)
  end

  def weather_params
    # whitelist params
    params.permit(:longitude, :latitude)
  end
end
