class TemperatureInfoGateway
  include HTTParty

  base_uri 'http://localhost:6655'

  def get_temperature(city)
    response = self.class.get(
      '/api/temperature',
      query: { city: city }
    )
    if response.success?
      response.parsed_response
    else
      {}
    end
  end
  end
