class CityFinderGateway
  include HTTParty

  base_uri 'http://localhost:5566'

  def get_cities(longitude, latitude)
    response = self.class.get(
      '/api/cities',
      query: { longitude: longitude, latitude: latitude }
    )
    if response.success?
      response.parsed_response
    else
      []
    end
  end
end
