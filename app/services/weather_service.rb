class WeatherService
  def initialize(longitude, latitude)
    @longitude = longitude
    @latitude = latitude
  end

  def call
    @weather_list = []
    cities.each do |_city|
      @weather_list << { city: _city, temperature: temparture4city(_city)["temperature"] }
    end
    @weather_list
  end

  private

  def cities
    CityFinderGateway.new.get_cities(@longitude, @latitude)
  end

  def temparture4city(city)
    TemperatureInfoGateway.new.get_temperature(city)
  end
end
