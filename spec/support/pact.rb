require 'pact/consumer/rspec'
require 'find_a_port'

RSpec.configure do |_config|
  Pact.service_consumer 'weather' do
    has_pact_with 'city_finder' do
      mock_service :pact_cityfinder_gateway do
        port FindAPort.available_port
        pact_specification_version '2.0.0'
      end
    end
  end

  Pact.service_consumer 'weather' do
    has_pact_with 'temperature_info' do
      mock_service :pact_temperature_gateway do
        port FindAPort.available_port
        pact_specification_version '2.0.0'
      end
    end
  end
end
