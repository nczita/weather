# spec/requests/weather_spec.rb
require 'rails_helper'

RSpec.describe 'Weather API', type: :request, pact: true do
  before do
    CityFinderGateway.base_uri pact_cityfinder_gateway.mock_service_base_url
    TemperatureInfoGateway.base_uri pact_temperature_gateway.mock_service_base_url
  end

  # Test suite for GET /weather
  describe 'GET /weather' do
    # make HTTP get request before each example
    before do
      get '/weather'
    end

    it 'returns nothing' do
      expect(json).to be_empty
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /weather?longitude=NNN&latitude=KKKK
  describe 'GET /weather?longitude=:longitude&latitude=:latitude' do
    let(:longitude) { 54.3520500 }
    let(:latitude) { 18.6463700 }

    before do
      pact_cityfinder_gateway
        .given('have cities')
        .upon_receiving('a request for cities of longitude and latitude')
        .with(
          method: 'get',
          path: '/api/cities',
          query: {
            longitude: like('54.35205'),
            latitude: like('18.64637')
          },
          headers: {
            'Accept' => '*/*'
          }
        )
        .will_respond_with(
          status: 200,
          headers: {
            'Content-Type' =>
              term(generate: 'application/json; charset=utf-8', matcher: %r{application/json})
          },
          # TODO: eachlike
          body: %w[
            Gdansk Sopot Wejherowo Sztum Gdynia
          ]
        )
      pact_temperature_gateway
        .given('know temperature')
        .upon_receiving('get temperature for city')
        .with(
          method: 'get',
          path: '/api/temperature',
          query: { city: term(generate: 'Sopot', matcher: /[a-zA-Z]+/) }
        )
        .will_respond_with(
          status: 200,
          headers: {
            'Content-Type' =>
              term(generate: 'application/json; charset=utf-8', matcher: %r{application/json})
          },
          body: like(
            city: 'Sopot',
            temperature: like(3)
          )
        )
      get "/weather?longitude=#{longitude}&latitude=#{latitude}"
    end
    context 'when the weather report exists' do
      it 'returns the weather report' do
        expect(json).not_to be_empty
        expect(json.size).to eq(5)
        expect(json[0]['city']).to eq('Gdansk')
        expect(json[0]['temperature']).to eq(3)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
  describe 'GET /weather?longitude=:longitude&latitude=:latitude error' do
    context 'when the weather report cannot be generated' do
      let(:longitude) { 154.3520500 }
      let(:latitude) { 198.6463700 }

      before do
        get "/weather?longitude=#{longitude}&latitude=#{latitude}"
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns an error' do
        expect(json).not_to be_empty
        expect(json['error']).to eq('Invalid params')
        expect(json['id']).to eq('CKE-123')
      end
    end
  end
end
