require 'rails_helper'
require_relative 'weather_states'

def provider_honours_pact_with(consumer_name, provider_name)
  Pact.service_provider provider_name do
    honours_pact_with consumer_name do
      if ENV['PACT_BROKER_URL']
        pact_uri "#{ENV['PACT_BROKER_URL']}/pacts/provider/#{provider_name}/consumer/#{consumer_name}/latest"
      else
        pact_uri "spec/pacts/#{consumer_name}-#{provider_name}.json"
      end
    end
  end
end

provider_honours_pact_with('weather', 'city_finder')
provider_honours_pact_with('weather', 'temperature_info')
