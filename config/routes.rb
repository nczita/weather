Rails.application.routes.draw do
  get 'weather', to: 'weather#show'

  get '/api/cities', to: 'cities#show'
  get '/api/temperature', to: 'temperature#show'
end
